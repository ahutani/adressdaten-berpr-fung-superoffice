Licence agreement

SUPEROFFICE LICENCE AGREEMENT

LIMITED WARRANTY – SuperOffice warrants that (a) the SOFTWARE PRODUCT will perform substantially in accordance with the accompanying written materials for a period of ninety (90) days from the date of receipt, and (b) any
Support Services provided by SuperOffice shall be substantially as described in applicable written materials provided to you by SuperOffice, and SuperOffice support engineers will make commercially reasonable efforts to solve
any problem issues. Some states and jurisdictions do not allow limitations on duration of an implied warranty, so the above limitation may not apply to you. To the extent allowed by applicable law, implied warranties on the
SOFTWARE PRODUCT, if any, are limited to ninety (90) days.

CUSTOMER REMEDIES – SuperOffice's and its suppliers' entire liability and your exclusive remedy shall be, at SuperOffice's option, either (a) return of the price paid, if any, or (b) repair or replacement of the SOFTWARE PRODUCT
that does not meet SuperOffice's Limited Warranty and which is retumed to SuperOffice with a copy of your receipt. This Limited Warranty is void if failure of the SOFTWARE PRODUCT has resulted from accident, abuse, or
misapplication. Any replacement SOFTWARE PRODUCT will be warranted for the remainder of the original warranty period or thirty (30) days, whichever is longer. Outside the United States, neither these remedies nor any
product support services offered by SuperOffice are available without proof of purchase from an authorized international source.

NO OTHER WARRANTIES – TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, SUPEROFFICE AND ITS SUPPLIERS DISCLAIM ALL OTHER WARRANTIES AND CONDITIONS, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT WITH REGARD TO THE SOFTWARE PRODUCT, AND THE PROVISION OF OR FAILURE TO PROVIDE
SUPPORT SERVICES. THIS LIMITED WARRANTY GIVES YOU SPECIFIC LEGAL RIGHTS. YOU MAY HAVE OTHERS, WHICH VARY FROM STATE/JURISDICTION TO STATE/JURISDICTION.

LIMITATION OF LIABILITY – TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, SUPEROFFICE OR ITS SUPPLIERS SHALL IN NO EVENT BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES
WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR
INABILITY TO USE THE SOFTWARE PRODUCT OR THE PROVISION OF OR FAILURE TO PROVIDE SUPPORT SERVICES, EVEN IF SUPEROFFICE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, SUPEROFFICE 'S
ENTIRE LIABILITY UNDER ANY PROVISION OF THIS AGREEMENT SHALL BE LIMITED TO THE GREATER OF THE AMOUNT ACTUALLY PAID BY YOU FOR THE SOFTWARE PRODUCT OR AN AMOUNT EQUIVALENT TO USD 5.oo IN LOCAL
CURRENCY; PROVIDED, HOWEVER, IF YOU HAVE ENTERED INTO A SUPEROFFICE SUPPORT SERVICES AGREEMENT; SUPEROFFICE'S ENTIRE LIABILITY REGARDING SUPPORT SERVICES SHALL BE GOVERNED BY THE TERMS OF THAT
AGREEMENT. BECAUSE SOME STATES AND JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.

GENERAL TERMS – The SOFTWARE PRODUCT is designed and offered as a general-purpose product, not for any user's particular purpose. You accept that no SOFTWARE PRODUCT is error-free. The above warranty and liability
limitations do not apply in the case of direct personal injury or death caused by the negligence of SuperOffice. This Agreement is governed by the laws of England if you acquired the Product in the United Kingdom. This
Agreement is governed by the laws of the Republic of Ireland if you acquired this Product in the Republic of Ireland.

Should you have any questions concerning this Agreement, or if you desire to contact SuperOffice for any reason, write to:

SuperOffice ASA, P.B. 1884 Vika, 0124 Oslo, Norway