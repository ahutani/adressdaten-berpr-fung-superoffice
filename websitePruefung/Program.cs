﻿ using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using SuperOffice.CRM.Rows;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using HtmlAgilityPack;

namespace websitePruefung
{
    public class Program
    {
        //Variable for website available or not
        private const int ContIntId_WebsiteIsAvailable = 7;
        private const int ContIntId_WebsiteIsNOTAvailable = 8;
        //Variables for Impressum found or not found
        private const int ContIntId_ImpressumFound = 9;
        private const int ContIntId_ImpressumNOTFOund = 10;
        //Variable for companyName (matches or doesn't)
        private const int ContIntId_matchingImpressum_Name = 11;
        private const int ContIntId_matchingImpressum_NameNOTMatch = 13;
        //Variables for postalcode (match or doesn't)
        private const int ContIntId_matchingImpressum_POBox = 14;
        private const int ContIntId_matchingImpressum_POBoxNOTMatch = 15;
        //Variables for street address
        private const int ContIntId_matchingImpressum_street = 16 ;
        private const int ContIntId_matchingImpressum_streetNOTMatch = 17;
        //Variable for city 
        private const int ConIntId_matchingImpressum_city = 19;
        private const int ConIntId_matchingImpressum_cityNOTMAtch = 20;
        //Variables for indicating that everything perfectly matches
        private const int ContIntId_matchingImpressum_OKEY = 18;

        private List<string> impressumAlternatives = new List<string>() { "impressum", "kontakt" };


        //Adding URL-Impressum 
        static void AddURLImpressum(int contactId, string urlImpressum)
        {
            //AddressRow.GetFromIdxOwnerIdAtypeIdx(contact.ContactId, SuperOffice.Data.AddressType.ContactPostalAddress);
            var checkURLRow = URLRows.GetFromIdxUrlAddress(urlImpressum);                       
            if (checkURLRow != null )
            {
                var contactURL = URLRow.CreateNew();
                contactURL.ContactId = contactId;                
                contactURL.UrlAddress = urlImpressum;
                contactURL.Save();

            }                          
        }
        static void AddContactInterest(int contactId, int contIntId)
        {

            var checkRow = ContactInterestRow.GetFromIdxContactIdCinterestIdx(contactId, contIntId);
            if (checkRow == null || !checkRow.IsNew)
            {
                Console.WriteLine("Interest {0} for contact {1} was already set!", contIntId, contactId);
            }
            else
            {
                var contint = ContactInterestRow.CreateNew();                
                contint.CinterestIdx = contIntId;
                contint.ContactId = contactId;
                contint.EndDate = DateTime.MaxValue;
                contint.StartDate = DateTime.MinValue;
                contint.Save();
                Console.WriteLine("Interest {0} for contact {1} just has been set!", contIntId, contactId);
            }
        } 
        static void RemoveContactInterest(int contactId, int contIntId)
        {            
            var contint = ContactInterestRow.GetFromIdxContactIdCinterestIdx(contactId, contIntId);
            if(contint != null && !contint.IsNew)
            {
                contint.Delete();
            }                
        }               
        static void HandleContactWithUrlError(SuperOffice.CRM.Rows.ContactRow contact,string url, string error)
        {
            Console.WriteLine(" ");
            Console.Write("{0} --> {1}", contact.Name, url);
            Console.WriteLine(error);           
            AddContactInterest(contact.ContactId, ContIntId_WebsiteIsNOTAvailable);
            RemoveContactInterest(contact.ContactId, ContIntId_WebsiteIsAvailable);
            //If URL is not available then Impressum out of the question
            RemoveContactInterest(contact.ContactId, ContIntId_ImpressumFound);
            RemoveContactInterest(contact.ContactId, ContIntId_ImpressumNOTFOund);
            RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_NameNOTMatch);            
            RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_Name);           
        }        
        static void HandleContactWithNOUrlError(SuperOffice.CRM.Rows.ContactRow contact, string url, string messageresponse)
        {
            Console.WriteLine(" ");
            Console.Write("{0} --> {1}", contact.Name, url);
            Console.WriteLine(" --> Response " + messageresponse);                       
            AddContactInterest(contact.ContactId, ContIntId_WebsiteIsAvailable);
            RemoveContactInterest(contact.ContactId, ContIntId_WebsiteIsNOTAvailable);
        }
                              
        static async Task Main(string[] args)
        {
            HttpClient client = new HttpClient();
            try
            {
                Console.WriteLine("log in to superoffice...");
                using (SuperOffice.SoSession session = SuperOffice.SoSession.Authenticate("AH","asdf1234"))
                {
                    Console.WriteLine("logged in.");
                    // GET ALL CONTACTS
                    SuperOffice.CRM.Rows.ContactRows.CustomSearch q = new SuperOffice.CRM.Rows.ContactRows.CustomSearch();
                    q.RestrictionAnd(q.TableInfo.ContactId.GreaterThan(0));
                    SuperOffice.CRM.Rows.ContactRows contacts = SuperOffice.CRM.Rows.ContactRows.GetFromCustomSearch(q);
                    
                    // LOOP TROUGH ALL CONTACTS
                    foreach (SuperOffice.CRM.Rows.ContactRow contact in contacts)
                    {
                        // GET ALL URLS FROM CONTACT AND IF THERE ARE NONE SKIP
                        SuperOffice.CRM.Rows.URLRows urls = SuperOffice.CRM.Rows.URLRows.GetFromIdxContactId(contact.ContactId);
                        if (urls.Count == 0)
                        {
                            continue;
                        }

                        // GET URL AND IF NO HTTP IN FRONT THEN ADD IT
                        string url = urls[0].UrlAddress;
                        if (!url.StartsWith("http", StringComparison.OrdinalIgnoreCase))
                        {
                            url = "http://" + url;
                        }

                        try
                        {
                            // GET THE PAGE RESULT
                            var result = await client.GetAsync(url);

                            // IF THERE IS SOME ERROR THEN HANDLE THE ERROR
                            if (result.StatusCode != System.Net.HttpStatusCode.OK)
                            {
                                HandleContactWithUrlError(contact, url, result.StatusCode.ToString());
                            }
                            else
                            {
                                HandleContactWithNOUrlError(contact, url, result.StatusCode.ToString());
                                await CheckImpressum(client, contact, url, result);
                                //HandleContactwithImpressum(contact, address, href_impressumLists, impressumContentname, impressumContentPOBOx, matches);
                            }
                        }
                        catch (Exception ex)
                        {
                            HandleContactWithUrlError(contact, url, " Error Message : " + ex.Message);
                        }
                    }
                                                         
                }
            }
            catch (SuperOffice.Exceptions.SoSessionException soEx)
            {
                Console.WriteLine(" Unable to log in to superoffice database. details:" + soEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(" --> Unknown error : " + ex.Message); 
            }            
            Console.ReadLine();
        }

        private static bool ContainsIgnoreCase(string source, string search)
        {
            return source.ToLower().Contains(search.ToLower());
        }

        private static void CheckContactDetailsInImpressumBody(ContactRow contact, string body)
        {
            var postalAddress = AddressRow.GetFromIdxOwnerIdAtypeIdx(contact.ContactId, SuperOffice.Data.AddressType.ContactPostalAddress);
            var streetAddress = AddressRow.GetFromIdxOwnerIdAtypeIdx(contact.ContactId, SuperOffice.Data.AddressType.ContactStreetAddress);
            
            

            bool foundAllInfos = true;
            if (ContainsIgnoreCase(body, contact.Name))
            {
                // Impressum contains the company name
                // ADD INTEREST FOR COMPANY NAME FOUND!
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_Name);
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_NameNOTMatch);
            }
            else
            {
                // Impressum does not contain the company name
                // REMOVE INTEREST - FOR COMPANY NAME FOUND!
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_NameNOTMatch);
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_Name);                              
                foundAllInfos = false;
            }

            
            if (ContainsIgnoreCase(body, streetAddress.Address1) &&
                ContainsIgnoreCase(body, streetAddress.Address2) &&
                ContainsIgnoreCase(body, streetAddress.Address3))

            {
                // Impressum contains the address
                // ADD INTEREST FOR ADDRESS FOUND!
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_street);
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_streetNOTMatch);
            }
            else
            {
                // Impressum does not contain the street address
                // ADD INTEREST FOR STREET ADDRESS NOT FOUND!
                // REMOVE INTEREST FOR STREET ADDRESS FOUND!  
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_street);
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_streetNOTMatch);
                foundAllInfos = false;
            }


            if (ContainsIgnoreCase(body, streetAddress.City))
            {
                // Impressum contains the city address
                // ADD INTEREST FOR CITY ADDRESS FOUND!
                AddContactInterest(contact.ContactId, ConIntId_matchingImpressum_city);
                RemoveContactInterest(contact.ContactId, ConIntId_matchingImpressum_cityNOTMAtch);
            }
            else 
            {
                // Impressum does not contain the city address
                // ADD INTEREST FOR CITY ADDRESS NOT FOUND!
                // REMOVE INTEREST FOR CITY ADDRESS FOUND!  
                RemoveContactInterest(contact.ContactId, ConIntId_matchingImpressum_city);
                AddContactInterest(contact.ContactId, ConIntId_matchingImpressum_cityNOTMAtch);
                foundAllInfos = false;
            }


            if (ContainsIgnoreCase(body, postalAddress.Zipcode))
            {
                // Impressum contains the postal address
                // ADD INTEREST FOR POSTAL ADDRESS FOUND!
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_POBox);
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_POBoxNOTMatch);
            }
            else 
            {
                // Impressum does not contain the postal address
                // ADD INTEREST FOR POSTAL ADDRESS NOT FOUND!
                // REMOVE INTEREST FOR POSTAL ADDRESS FOUND!  
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_POBox);
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_POBoxNOTMatch);
                foundAllInfos = false;
            }            
                                                                                               
            if(foundAllInfos == true)
            {
            // ADD INTEREST FOR ALL CHECKS OK!
                AddContactInterest(contact.ContactId, ContIntId_matchingImpressum_OKEY);
            }
            else
            {
             // REMOVE INTEREST FOR ALL CHECKS OK!
                RemoveContactInterest(contact.ContactId, ContIntId_matchingImpressum_OKEY);
            }
        }

        private static async Task CheckImpressum(HttpClient client, ContactRow contact, string url, HttpResponseMessage result)
        {
            Console.WriteLine("Searching for Impressum in content: ");
            Regex regex = new Regex(@"\s*href\s*=\s*\""([^""]*)[^>]*>([^<]*)", RegexOptions.IgnoreCase);
            string body = await result.Content.ReadAsStringAsync();

            //Try to match each line against the regex                                                                
            var matches = regex.Matches(body);
            //Counter variable to show index of iteration
            int row = 0;

            // eventuell später auch die street address prüfen?
           
            foreach (Match match in matches)
            {
                //Write original line and the value
                string urlAttr = match.Groups[1].Value;
                string innerText = match.Groups[2].Value;

                if (match.Success)
                {
                    if (urlAttr.ToLower().Contains("impressum") || innerText.ToLower().Contains("impressum"))
                    {
                        // IF URL IS /impressum or impressum/ then prepend url so its http://test/impressum/
                        if (urlAttr.StartsWith("/") || urlAttr.EndsWith("/") || urlAttr.EndsWith(".html"))
                        {
                            urlAttr = url + urlAttr;
                        }
                        Console.WriteLine("Match found from URL " + urlAttr + ", at row " + row + ", in " + matches);
                        //checking inside the impressum    
                        Console.WriteLine("Review the content inside the impressum:");
                        var result_impressum = await client.GetAsync(urlAttr);
                        
                        if(result_impressum.StatusCode != System.Net.HttpStatusCode.OK)
                        {
                            // IMPRESSUM WASNT FOUND
                            RemoveContactInterest(contact.ContactId, ContIntId_ImpressumFound);
                            AddContactInterest(contact.ContactId, ContIntId_ImpressumNOTFOund);
                        }
                        else
                        {
                            // IMPRESSUM WAS FOUND!
                            AddContactInterest(contact.ContactId, ContIntId_ImpressumFound);
                            RemoveContactInterest(contact.ContactId, ContIntId_ImpressumNOTFOund);
                            string body_impressum = await result_impressum.Content.ReadAsStringAsync();
                            CheckContactDetailsInImpressumBody(contact, body_impressum);

                            // ADD URL IMPRESSUM INTO THE FIELD WEBSITE OF COMPANY REFERENCE DATA (FIRMENSTAMMDATEN) IN SUPEROFFICE                             
                            AddURLImpressum(contact.ContactId, urlAttr);
                        }
                    }
                }
                row++; ;
            }
            Console.WriteLine("Iteration finished. ");
        }
    }
}
